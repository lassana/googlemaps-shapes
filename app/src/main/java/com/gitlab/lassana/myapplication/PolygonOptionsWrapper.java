package com.gitlab.lassana.myapplication;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

/**
 * @author Nikolai Doronin {@literal <lassana.nd@gmail.com>}
 * @since 4/7/16.
 */
public class PolygonOptionsWrapper {

    public enum Behavior {
        FULL_SHOWING, PART_SHOWING
    }

    private final PolygonOptions options;
    private final Behavior behavior;

    private Polygon polygonReference;

    private final LatLng northWest, northEast, southEast, southWest;

    public void addTo(GoogleMap map) {
        if (isAdded()) removeFrom(map);
        polygonReference = map.addPolygon(options);
    }

    public void removeFrom(GoogleMap map) {
        if (!isAdded()) return;
        polygonReference.remove();
        polygonReference = null;
    }

    public interface PolygonOptionsCreator {
        PolygonOptions create();
    }

    public PolygonOptionsWrapper(PolygonOptionsCreator creator, Behavior behavior) {
        this(creator.create(), behavior);
    }

    public PolygonOptionsWrapper(PolygonOptions options, Behavior behavior) {
        this.options = options;
        this.behavior = behavior;
        Double north = null, west = null, south = null, east = null;
        for (LatLng latLng : options.getPoints()) {
            if (north == null || latLng.latitude > north) {
                north = latLng.latitude;
            }
            if (west == null || latLng.longitude < west) {
                west = latLng.longitude;
            }
            if (south == null || latLng.latitude < south) {
                south = latLng.latitude;
            }
            if (east == null || latLng.longitude > east) {
                east = latLng.longitude;
            }
        }
        northWest = new LatLng(north, west);
        northEast = new LatLng(north, east);
        southEast = new LatLng(south, east);
        southWest = new LatLng(south, west);
    }

    public PolygonOptions getOptions() {
        return options;
    }

    public PolygonOptions buildBordersRectPolygonOptions() {
        final PolygonOptions rvalue = new PolygonOptions();
        rvalue.add(northWest);
        rvalue.add(northEast);
        rvalue.add(southEast);
        rvalue.add(southWest);
        rvalue.add(northWest);
        rvalue.fillColor(0x6A00FFFF);
        rvalue.strokeColor(0x6AFF0000);
        rvalue.strokeWidth(1f);
        return rvalue;
    }

    public boolean within(LatLngBounds bounds) {
        boolean within = false;
        switch (behavior) {
            case FULL_SHOWING:
                if (bounds.contains(northWest) && bounds.contains(southEast)) {
                    within = true;
                }
                break;
            case PART_SHOWING:
                if (bounds.contains(northWest)
                        || bounds.contains(southEast)
                        || bounds.contains(northEast)
                        || bounds.contains(southWest)) {
                    within = true;
                } else if (northEast.latitude > bounds.southwest.latitude
                        && northEast.longitude > bounds.southwest.longitude
                        && southWest.latitude < bounds.northeast.latitude
                        && southWest.longitude < bounds.northeast.longitude) {
                    within = true;
                }
                break;
        }
        return within;
    }

    public boolean isAdded() {
        return polygonReference != null;
    }
}
