package com.gitlab.lassana.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap map;

    private PolygonOptionsWrapper[] wrappers = {
            new PolygonOptionsWrapper(new PolygonOptionsWrapper.PolygonOptionsCreator() {
                @Override
                public PolygonOptions create() {
                    final PolygonOptions options = new PolygonOptions();
                    final List<LatLng> points = new ArrayList<>();
                    points.add(new LatLng(51.99, 23.70));
                    points.add(new LatLng(52.09, 30.65));
                    points.add(new LatLng(53.43, 31.35));
                    points.add(new LatLng(54.46, 26.96));
                    points.add(new LatLng(54.18, 25.6));
                    options.addAll(points);
                    options.fillColor(Color.LTGRAY);
                    options.strokeColor(Color.GREEN);
                    options.strokeWidth(2f);
                    return options;
                }
            }, PolygonOptionsWrapper.Behavior.PART_SHOWING),
            new PolygonOptionsWrapper(new PolygonOptionsWrapper.PolygonOptionsCreator() {
                @Override
                public PolygonOptions create() {
                    final PolygonOptions options = new PolygonOptions();
                    final List<LatLng> points = new ArrayList<>();
                    points.add(new LatLng(51.80, 19.06));
                    points.add(new LatLng(52.86, 13.27));
                    points.add(new LatLng(46.93, 10.01));
                    points.add(new LatLng(45.20, 17.84));
                    options.addAll(points);
                    options.fillColor(Color.LTGRAY);
                    options.strokeColor(Color.GREEN);
                    options.strokeWidth(2f);
                    return options;
                }
            }, PolygonOptionsWrapper.Behavior.PART_SHOWING)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                showPolygons();
            }
        });
        showPolygons();
    }

    private void showPolygons() {
        if (map == null) return;
        final LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
        for (PolygonOptionsWrapper wrapper : wrappers) {
            if (wrapper.within(bounds)) {
                if (!wrapper.isAdded()) {
                    wrapper.addTo(map);
                }
            } else {
                if (wrapper.isAdded()) {
                    wrapper.removeFrom(map);
                }
            }
        }
    }
}
